# -*- coding: utf-8 -*-
from django.urls import path

from . import views

urlpatterns = [
    path('book/', views.handleBookRequest, name = 'handleBookRequest'),
    path('book',  views.handleBookRequest, name = 'handleBookRequest'),

    path('book/<str:reqIsbn>',  views.handleSpecificBookRequest, name = 'handleSpecificBookRequest'),
    path('book/<str:reqIsbn>/', views.handleSpecificBookRequest, name = 'handleSpecificBookRequest'),

    path('bookDetail/<str:reqIsbn>',  views.handleBookDetailRequest, name = 'getBookDetails'),
    path('bookDetail/<str:reqIsbn>/', views.handleBookDetailRequest, name = 'getBookDetails'),

    path('', views.getEmptyResponse, name = 'pathNotFound')
]
