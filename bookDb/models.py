from django.db import models
from django.utils import timezone
import datetime

# Create your models here.

class Book(models.Model):
    isbn        = models.CharField("ISBN",   max_length = 20, null = False, primary_key = True, )
    book_name   = models.CharField("Name",   max_length = 70, null=False)
    author_name = models.CharField("Author", max_length = 70, null=False)
    def __str__(self):
        return self.book_name

class BookDetails(models.Model):
    book         = models.OneToOneField(Book, on_delete = models.CASCADE)
    num_pages    = models.PositiveIntegerField("Number of pages", default = 0, null = False)
    genre        = models.CharField("Genre", max_length = 70, null = False)
    ratings      = models.DecimalField("Ratings", default = 0, max_digits = 2, decimal_places = 1, null = False)
    pub_year     = models.PositiveIntegerField("Published Year", null = False)
    price        = models.CharField("Price", max_length = 70, null = False)
    def __str__(self):
        return self.book.book_name
