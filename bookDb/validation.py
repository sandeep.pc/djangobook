import re

def isIsbnValid(isbn):
    '''
     An ISBN is an International Standard Book Number. ISBNs were 10 digits in length up to the end of December 2006, but since 1 January 2007 they now always consist of 13 digits. ISBNs are calculated using a specific mathematical formula and include a check digit to validate the number.

 Each ISBN consists of 5 elements with each section being separated by spaces or hyphens. Three of the five elements may be of varying length:

 Prefix element – currently this can only be either 978 or 979. It is always 3 digits in length
 Registration group element – this identifies the particular country, geographical region, or language area participating in the ISBN system. This element may be between 1 and 5 digits in length
 Registrant element - this identifies the particular publisher or imprint. This may be up to 7 digits in length
 Publication element – this identifies the particular edition and format of a specific title. This may be up to 6 digits in length
 Check digit – this is always the final single digit that mathematically validates the rest of the number. It is calculated using a Modulus 10 system with alternate weights of 1 and 3.
 '''
    isbnRegex = r'^(978-?|979-?)?\d(-?\d){9}$'
    pattern = re.compile(isbnRegex, re.UNICODE)

    for match in pattern.findall(isbn):
        return True

    return False


def isIsbnStrongValid(isbn):
    '''
     An ISBN is an International Standard Book Number. ISBNs were 10 digits in length up to the end of December 2006, but since 1 January 2007 they now always consist of 13 digits. ISBNs are calculated using a specific mathematical formula and include a check digit to validate the number.

 Each ISBN consists of 5 elements with each section being separated by spaces or hyphens. Three of the five elements may be of varying length:

 Prefix element – currently this can only be either 978 or 979. It is always 3 digits in length
 Registration group element – this identifies the particular country, geographical region, or language area participating in the ISBN system. This element may be between 1 and 5 digits in length
 Registrant element - this identifies the particular publisher or imprint. This may be up to 7 digits in length
 Publication element – this identifies the particular edition and format of a specific title. This may be up to 6 digits in length
 Check digit – this is always the final single digit that mathematically validates the rest of the number. It is calculated using a Modulus 10 system with alternate weights of 1 and 3.
 
 Note: Need to test for more edge cases
 '''
    if len(isbn.replace('-', '')) != 13 or len(isbn.replace('-', '')) != 10:
        return False

    isbnRegex = r'^(978-?|979-?)?\d{1,5}-?\d{1,7}-?\d{1,6}-?\d{1,3}$'
    pattern = re.compile(isbnRegex, re.UNICODE)

    for match in pattern.findall(isbn):
        return True

    return False

def isFeildEmpty(field):
    if field == "":
        return True
    return False

def isValidPageSize(num_page):
    if num_page > 0 and num_page <= 3000:
        return True
    return False

def isValidGenre(genre):
    if genre == "" or len(genre) > 70:
        return False
    return True

def isValidRating(rating):
    if rating >= 0 and rating <= 5:
        return True
    return False

def isValidPubYear(pub_year):
    if pub_year >= 1000 and pub_year <= 2020:
        return True
    return False

def isValidPrice(price):
    if price == "" or len(price) > 20:
        return False
    return True