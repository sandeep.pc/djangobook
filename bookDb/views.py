from django.shortcuts import render
from django.http import JsonResponse

import json

from . models import Book, BookDetails
from . import validation

# Handles CRUD related to book


def handleBookRequest(request):
    if request.method    == 'POST':
        return addBook(request)
    elif request.method  == 'GET':
        return getBooks(request)
    else:
        return getErrorResponse('Unknown method : Not associated with this path')


def handleSpecificBookRequest(request, reqIsbn):
    if request.method   == 'GET':
        return getFullBookDetails(request, reqIsbn)
    elif request.method == 'PUT':
        return updateBook(request, reqIsbn)
    elif request.method == 'DELETE':
        return deleteBook(request, reqIsbn)
    else:
        return getErrorResponse('Unknown method : Not associated with this path')


def handleBookDetailRequest(request, reqIsbn):
    if request.method == 'POST':
        return addBookDetails(request, reqIsbn)
    elif request.method == 'PUT':
        return updateBookDetails(request, reqIsbn)
    else:
        return getErrorResponse()


def addBook(request):

    reqBody = json.loads(request.body.decode("utf-8"))

    # Validate request
    if reqBody.get('isbn', "") == "":
        return getErrorResponse("Please add ISBN number : isbn")
    if not validation.isIsbnValid(reqBody.get('isbn')):
        return getErrorResponse("Please add valid 13 digit ISBN number")
    if reqBody.get('book_name', "") == "":
        return getErrorResponse("Please add Book Name : book_name")
    if reqBody.get('author_name', "") == "":
        return getErrorResponse("Please add Author Name : author_name")

    # If valid check if already exist
    try:
        Book.objects.get(isbn = reqBody.get('isbn').replace('-',''))
        return getErrorResponse("Book already added!!")
    except Book.DoesNotExist:
        newBook = Book(
            isbn = reqBody['isbn'].replace('-',''), book_name = reqBody['book_name'], author_name = reqBody['author_name'])
        newBook.save()
    return getBooks(request)


def getBooks(request):

    # Try to fetch all records if not empty otherwise return error
    books = Book.objects.all()[:50].values()
    booksList = list(books)

    return getSuccessResponse(booksList)

def getFullBookDetails(request, reqIsbn):

    if not validation.isIsbnValid("" + reqIsbn):
        return getErrorResponse("Invalid 13 digit ISBN number" + reqIsbn)
    
    try:
        book = Book.objects.get(isbn = reqIsbn)

        data = {
            'isbn': book.isbn,
            'book_name': book.book_name,
            'author_name': book.author_name,
            }

        try :
            bookDetail = BookDetails.objects.get(book__isbn = reqIsbn)
            data['num_pages'] = bookDetail.num_pages
            data['genre'] = bookDetail.genre
            data['ratings'] = bookDetail.ratings
            data['pub_year'] = bookDetail.pub_year
            data['price'] = bookDetail.price

        except BookDetails.DoesNotExist:
            return getSuccessResponse(data)

        return getSuccessResponse(data)
    except Book.DoesNotExist:
        return getErrorResponse( "Book with ISBN " + reqIsbn + " does not exist!")




def updateBook(request, reqIsbn):

    reqBody = json.loads(request.body.decode("utf-8"))

    # Validate request
   
    if not validation.isIsbnValid("" + reqIsbn):
        return getErrorResponse("Invalid 13 digit ISBN number" + reqIsbn)
    if reqBody.get('book_name', "")   == "":
        return getErrorResponse("Please add Book Name : book_name")
    if reqBody.get('author_name', "") == "":
        return getErrorResponse("Please add Author Name : author_name")

    # If valid check if already exist
    try:
        book = Book.objects.get(isbn = reqIsbn)

        book.book_name   = reqBody.get('book_name')
        book.author_name = reqBody.get('author_name')
        book.save()

        responseData = { 'isbn' : book.isbn, 'book_name' : book.book_name, 'author_name' : book.author_name}
        return getSuccessResponse(responseData, "Book with ISBN " + book.isbn + " successfully updated!")
    except Book.DoesNotExist:
        return getErrorResponse("No book with specified ISBN found!")


def deleteBook(request, reqIsbn):

   # If valid check if exist

    if not validation.isIsbnValid(reqIsbn):
       return getErrorResponse("Invalid 13 digit ISBN number")

    try:
        book = Book.objects.get(isbn = reqIsbn)
        book.delete()
        return getSuccessResponse({}, "Book with ISBN " + reqIsbn + " successfully deleted!")
    except Book.DoesNotExist:
        return getErrorResponse( "Book with ISBN " + reqIsbn + " does not exist!")


# Book details 

def addBookDetails(request, reqIsbn):
    
    reqBody = json.loads(request.body.decode("utf-8"))

    if not validation.isIsbnValid(reqIsbn):
       return getErrorResponse("Invalid 13 digit ISBN number")

    if reqBody.get('num_pages', 0) == 0:
        return getErrorResponse("Please add number of pages : num_pages")
    if not validation.isValidPageSize(reqBody.get('num_pages', 0)):
        return getErrorResponse("Please add valid number of pages (from 1 to 3000 max pages) : num_pages")

    if not validation.isValidGenre(reqBody.get('genre', "")):
        return getErrorResponse("Please add genre of book (max 70 chars): genre")

    if reqBody.get('ratings', -1) == -1:
        return getErrorResponse("Please add ratings : ratings")
    if not validation.isValidRating(reqBody.get('ratings', -1)):
        return getErrorResponse("Please add valid ratings from (0 to 5) : ratings")
        
    if reqBody.get('pub_year', 0) == 0:
        return getErrorResponse("Please add published year : pub_year")
    if not validation.isValidPubYear(reqBody.get('pub_year', 0)):
        return getErrorResponse("Please add valid published year from 1000 to 2020 ('YYYY'): pub_year")

    if reqBody.get('price', "") == "":
        return getErrorResponse("Please add price of book : price")
               
    if not validation.isValidPrice(reqBody.get('price', "")):
        return getErrorResponse("Please add valid price of book (max 20 chars): price")

    try:
        bookDetail = BookDetails.objects.get(book__isbn = reqIsbn)
        return getErrorResponse("Book details already added!!")
    except BookDetails.DoesNotExist:
        newBookDetail   = BookDetails(
            num_pages   = reqBody.get('num_pages'),
            genre       = reqBody.get('genre'),
            ratings     = round(reqBody.get('ratings'), 1),
            pub_year    = reqBody.get('pub_year'),
            price       = reqBody.get('price'),
            book_id     = reqIsbn
        )
        newBookDetail.save()
    return getBookDetailResponse(newBookDetail)

def updateBookDetails(request, reqIsbn):

    reqBody = json.loads(request.body.decode("utf-8"))

    if not validation.isIsbnValid(reqIsbn):
       return getErrorResponse("Invalid 13 digit ISBN number")

  
    if reqBody.get('num_pages', 0) == 0:
        return getErrorResponse("Please add number of pages : num_pages")
    if not validation.isValidPageSize(reqBody.get('num_pages', 0)):
        return getErrorResponse("Please add valid number of pages (from 1 to 3000 max pages) : num_pages")

    if not validation.isValidGenre(reqBody.get('genre', "")):
        return getErrorResponse("Please add genre of book (max 70 chars): genre")

    if reqBody.get('ratings', -1) == -1:
        return getErrorResponse("Please add ratings : ratings")
    if not validation.isValidRating(reqBody.get('ratings', -1)):
        return getErrorResponse("Please add valid ratings from (0 to 5) : ratings")
        
    if reqBody.get('pub_year', 0) == 0:
        return getErrorResponse("Please add published year : pub_year")
    if not validation.isValidPubYear(reqBody.get('pub_year', 0)):
        return getErrorResponse("Please add valid published year from 1000 to 2020 ('YYYY'): pub_year")

    if reqBody.get('price', "") == "":
        return getErrorResponse("Please add price of book : price")
               
    if not validation.isValidPrice(reqBody.get('price', "")):
        return getErrorResponse("Please add valid price of book (max 20 chars): price")


    try:
        bookDetail = BookDetails.objects.get(book__isbn = reqIsbn)

        bookDetail.num_pages = reqBody.get('num_pages')
        bookDetail.genre     = reqBody.get('genre')
        bookDetail.ratings   = round(reqBody.get('ratings'), 1)
        bookDetail.pub_year  = reqBody.get('pub_year')
        bookDetail.price     = reqBody.get('price')

        bookDetail.save()

    except BookDetails.DoesNotExist:
        return getErrorResponse("Book details does not exist!Please add before updating!")

    return getBookDetailResponse(bookDetail)

def getBookDetailResponse(bookDetail):
    data = {
        'isbn': bookDetail.book.isbn,
        'book_name': bookDetail.book.book_name,
        'author_name': bookDetail.book.author_name,
        'num_pages': bookDetail.num_pages,
        'genre': bookDetail.genre,
        'ratings': bookDetail.ratings,
        'pub_year': bookDetail.pub_year,
        'price': bookDetail.price
        }
    return getSuccessResponse(data)


def getSuccessResponse(data, message = 'Success'):
    responseData = {'status': 200, 'message': message, 'data': data}
    return JsonResponse(responseData, safe = False)


def getEmptyResponse(request):
    return getErrorResponse()


def getErrorResponse(message="Not found!!"):
    responseData = {'status': 404, 'message': message, 'data': {}}
    return JsonResponse(responseData, safe = False)
