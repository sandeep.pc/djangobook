
Django - Demo Project


RE : Commands Consolidated

Commands


Install python3.dmg

sudo easy_install pip

pip3 install Django

Install mysql-8.0.13

sudo nano /etc/paths
/usr/local/mysql/bin 
/usr/local/mysql/support-files

env

echo 'export PATH=/usr/local/mysql/bin:$PATH' >> ~/.bash_profile

echo 'export PATH=/usr/local/mysql/support-files:$PATH' >> ~/.bash_profile

. ~/.bash_profile

sudo chmod 7777 /usr/local/mysql/*

delete .pid and .err from /mysql/data/

sudo pip install mysqlclient

pip3 install pymysql

 ps aux | grep mysql

sudo kill pid

Stop mysql instance from system preference -> mysql -> stop instance

sudo mysql.server start

mysql -u root -p password

show databases;

CREATE DATABASE book_database;

Use book_database;

Show tabels;


python3 manage.py makemigrations

python3 manage.py makemigrations bookDb

python3 manage.py migrate

Python3 manage.py runserver

sudo pip install isbn_validator

-------------------------------------

Commands

Python install - By downloading python 26mb

sudo easy_install pip
pip3 install Django

django-admin startproject book  

python3 manage.py diffsettings displays differences between the current settings file and Django’s default settings.


python3 manage.py runserver     (optional )8080(port)

python3 manage.py startapp bookDb

root qwertyuiop

sudo /usr/local/mysql/support-files/mysql.server start

sudo pip install mysqlclient
pip3 install pymysql

Then, edit the __init__.py file in your project origin dir(the same as settings.py)
add:
import pymysql

pymysql.install_as_MySQLdb()


python3 manage.py migrate

python3 manage.py makemigrations bookDb


python3 manage.py sqlmigrate polls 0001

python3 manage.py migrate


Change your models (in models.py).
Run python manage.py makemigrations to create migrations for those changes
Run python manage.py migrate to apply those changes to the database.


python3 manage.py createsuperuser

Book

django-admin startproject book  - “book” name of project should not be django in-built component

book/
manage.py
book/
__init__.py
settings.py
urls.py
wsgi.py


* The outer mysite/ root directory is just a container for your project. Its name doesn’t matter to Django; you can rename it to anything you like.
* manage.py: A command-line utility that lets you interact with this Django project in various ways.

 Python looks in several places when you try to import a module. Specifically, it looks in all the directories defined in sys.path. This is just a list, and you can easily view it or modify it with standard list methods
 
 def - function
 every function is an object in python
 import module - includes the chunk of code from the module
 import sys - includes sys module which include sys.path which is an attribute containing list of path where python will search for the corresponding .py or other files
 sys.path.insert(index, ‘path’) - function in sys.path which inserts a path in the specified index of sys.path
 function includes - func.__doc__ attribute which contains the documentation of the function
 
 In Python, the definition is looser. Some objects have neither attributes nor methods, but they could. Not all objects are subclassable. But everything is an object in the sense that it can be assigned to a variable or passed as an argument to a function.
 
 def funcname :
 return , colon , indentation
 
 error- check, in python error - handle
 try, catch, throw   = try, except, raise
 
 Well, modules are objects, and all modules have a built-in attribute __name__. A module’s __name__ depends on how you’re using the module. If you import the module, then __name__ is the module’s filename, without a directory path or file extension.
 >>> import humansize
 >>> humansize.__name__
 'humansize'
 But you can also run the module directly as a standalone program, in which case __name__ will be a special default value, __main__. Python will evaluate this if statement, find a true expression, and execute the if code block. In this case, to print two values.
 c:\home\diveintopython3> c:\python31\python.exe humansize.py
 1.0 TB
 931.3 GiB
 
 WSGI is the Web Server Gateway Interface. It is a specification that describes how a web server communicates with web applications, and how web applications can be chained together to process one request.
 
 * The inner mysite/ directory is the actual Python package for your project. Its name is the Python package name you’ll need to use to import anything inside it (e.g. mysite.urls).
 * mysite/__init__.py: An empty file that tells Python that this directory should be considered a Python package. If you’re a Python beginner, read more about packages in the official Python docs.
 
 * mysite/settings.py: Settings/configuration for this Django project. Django settings will tell you all about how settings work.
 
 If you set DEBUG to False, you also need to properly set the ALLOWED_HOSTS setting.
 
 ALLOWED_HOSTS = ['www.example.com']
 DEBUG = False
 DEFAULT_FROM_EMAIL = 'webmaster@example.com'
 
 When you use Django, you have to tell it which settings you’re using. Do this by using an environment variable, DJANGO_SETTINGS_MODULE.
 The value of DJANGO_SETTINGS_MODULE should be in Python path syntax, e.g. mysite.settings. 
 
 Default settings¶
 A Django settings file doesn’t have to define any settings if it doesn’t need to. Each setting has a sensible default value. These defaults live in the module django/conf/global_settings.py.
 Here’s the algorithm Django uses in compiling settings:
 * Load settings from global_settings.py.
 * Load settings from the specified settings file, overriding the global settings as necessary.
 Note that a settings file should not import from global_settings, because that’s redundant
 
 python3 manage.py diffsettings displays differences between the current settings file and Django’s default settings.
 
 
 mysite/urls.py: The URL declarations for this Django project; a “table of contents” of your Django-powered site.
 
 mysite/wsgi.py: An entry-point for WSGI-compatible web servers to serve your project. 
 
 python3 manage.py runserver     (optional )8080(port)
 
 python3 manage.py startapp bookDb
 
 db.sqlite3
 
 bookDb/
 __init__.py      - Initialize module bookDb
 admin.py
 apps.py
 migrations/
 __init__.py  - initialize module bookDb.migrations
 models.py
 tests.py
 views.py
 
 sudo pip3 install -U Django
 
 International Standard Book Number
 
 django.db.backends.mysql
 'NAME': 'name',    
 'USER': 'username',
 'PASSWORD': 'password',
 'HOST': '',
 'PORT': '',
 
 By default, INSTALLED_APPS contains the following apps, all of which come with Django:
 * django.contrib.admin – The admin site. You’ll use it shortly.
 * django.contrib.auth – An authentication system.
 * django.contrib.contenttypes – A framework for content types.
 * django.contrib.sessions – A session framework.
 * django.contrib.messages – A messaging framework.
 * django.contrib.staticfiles – A framework for managing static files.
 
 A model is the single, definitive source of truth about your data. It contains the essential fields and behaviors of the data you’re storing. Django follows the DRY Principle. The goal is to define your data model in one place and automatically derive things from it.
 This includes the migrations - unlike in Ruby On Rails, for example, migrations are entirely derived from your models file, and are essentially just a history that Django can roll through to update your database schema to match your current models.
 
 
 from django.db import models
 
 # Create your models here.
 class Question(models.Model):
 question_text = models.CharField(max_length=200)
 pub_date = models.DateTimeField('date published')
 
 class Choice(models.Model):
 question = models.ForeignKey(Question, on_delete=models.CASCADE)
 choice_text = models.CharField(max_length=200)
 votes = models.IntegerField(default=0)
 
 That small bit of model code gives Django a lot of information. With it, Django is able to:
 * Create a database schema (CREATE TABLE statements) for this app.
 * Create a Python database-access API for accessing Question and Choice objects.
 But first we need to tell our project that the polls app is installed.
 
 
 To include the app in our project, we need to add a reference to its configuration class in the INSTALLED_APPSsetting. The PollsConfig class is in the polls/apps.py file, so its dotted path is 'polls.apps.PollsConfig'. Edit the mysite/settings.py file and add that dotted path to the INSTALLED_APPS setting. It’ll look like this:
 mysite/settings.py¶
 
 INSTALLED_APPS = [
 'polls.apps.PollsConfig',
 'django.contrib.admin',
 'django.contrib.auth',
 'django.contrib.contenttypes',
 'django.contrib.sessions',
 'django.contrib.messages',
 'django.contrib.staticfiles',
 ]
 
 
 python3 manage.py sqlmigrate polls 0001
 
 To check in MySQL query generation
 * Primary keys (IDs) are added automatically. (You can override this, too.)
 * By convention, Django appends "_id" to the foreign key field name. (Yes, you can override this, as well.)
 * The foreign key relationship is made explicit by a FOREIGN KEY constraint. Don’t worry about the DEFERRABLEparts; that’s just telling PostgreSQL to not enforce the foreign key until the end of the transaction.
 * It’s tailored to the database you’re using, so database-specific field types such as auto_increment (MySQL), serial (PostgreSQL), or integer primary key autoincrement (SQLite) are handled for you automatically. Same goes for the quoting of field names – e.g., using double quotes or single quotes.
 * The sqlmigrate command doesn’t actually run the migration on your database - it just prints it to the screen so that you can see what SQL Django thinks is required. It’s useful for checking what Django is going to do or if you have database administrators who require SQL scripts for changes.
 
 
 
 ython3 manage.py makemigrations bookDb
 
 By running makemigrations, you’re telling Django that you’ve made some changes to your models (in this case, you’ve made new ones) and that you’d like the changes to be stored as a migration.
 Migrations are how Django stores changes to your models (and thus your database schema) - they’re just files on disk. 
 You can read the migration for your new model if you like; it’s the filepolls/migrations/0001_initial.py.
 
 Migrations are very powerful and let you change your models over time, as you develop your project, without the need to delete your database or tables and make new ones - it specializes in upgrading your database live, without losing data. 
 
 polls/admin.py¶
 
 from django.contrib import admin
 
 from .models import Question
 
 admin.site.register(Question)


pip - python 2.7 pip3 - python 3.7

Python 3.7.1 - version

Python install - By downloading python 26mb

pip install Django - package management system used to install and manage software packages written in Python  - Python Package Index (PyPI).

sudo easy_install pip
pip3 install Django

django-admin startproject book - creates project

cd book

pip3 install django-tastypie

python3 manage.py startapp api

# notable_django/settings.py
INSTALLED_APPS = [
'django.contrib.admin',
'django.contrib.auth',
'django.contrib.contenttypes',
'django.contrib.sessions',
'django.contrib.messages',
'django.contrib.staticfiles',
'api' - to start our app
]

A model is the single, definitive source of information about your data. It contains the essential fields and behaviors of the data you’re storing. Generally, each model maps to a single database table.
* Each model is a Python class that subclasses django.db.models.Model.
* Each attribute of the model represents a database field.
* With all of this, Django gives you an automatically-generated database-access API; 

Adding model
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Book(models.Model):
ISBN = models.CharField(max_length=200)
body = models.TextField()
created_at = models.DateTimeField(auto_now_add=True)

def __str__(self):
return '%s %s' % (self.ISBN, self.body)


python3 manage.py makemigrations

python3 manage.py migrate

python3 manage.py shell

python3 manage.py shell
>>> from api.models import Note
>>> note = Note(title="First Note", body="This is certainly noteworthy")
>>> note.save()
>>> Note.objects.all()
<QuerySet [<Note: First Note This is certainly noteworthy>]>
>>> exit()


# api/resources.py
from tastypie.resources import ModelResource
from api.models import Note
class NoteResource(ModelResource):
class Meta:
queryset = Note.objects.all()
resource_name = 'note'


from django.conf.urls import url, include
from django.contrib import admin
from api.resources import NoteResource
note_resource = NoteResource()
urlpatterns = [
url(r'^admin/', admin.site.urls),
url(r'^api/', include(note_resource.urls)),
]


python3 manage.py runserver

What is an ISBN?  regex = r'978(?:-?\d){10}'



An ISBN is an International Standard Book Number. ISBNs were 10 digits in length up to the end of December 2006, but since 1 January 2007 they now always consist of 13 digits. ISBNs are calculated using a specific mathematical formula and include a check digit to validate the number.
Each ISBN consists of 5 elements with each section being separated by spaces or hyphens. Three of the five elements may be of varying length:
* Prefix element – currently this can only be either 978 or 979. It is always 3 digits in length 
* Registration group element – this identifies the particular country, geographical region, or language area participating in the ISBN system. This element may be between 1 and 5 digits in length 
* Registrant element - this identifies the particular publisher or imprint. This may be up to 7 digits in length
* Publication element – this identifies the particular edition and format of a specific title. This may be up to 6 digits in length 
* Check digit – this is always the final single digit that mathematically validates the rest of the number. It is calculated using a Modulus 10 system with alternate weights of 1 and 3.
What is an ISBN used for?
An ISBN is essentially a product identifier used by publishers, booksellers, libraries, internet retailers and other supply chain participants for ordering, listing, sales records and stock control purposes. The ISBN identifies the registrant as well as the specific title, edition and format.
What does an ISBN identify?
ISBNs are assigned to text-based monographic publications (i.e. one-off publications rather than journals, newspapers, or other types of serials).
Any book made publicly available, whether for sale or on a gratis basis, can be identified by ISBN.
In addition, individual sections (such as chapters) of books or issues or articles from journals, periodicals or serials that are made available separately may also use the ISBN as an identifier.
With regard to the various media available, it is of no importance in what form the content is documented and distributed; however, each different product form (e.g. paperback, EPUB, .pdf) should be identified separately.
You can find examples of types of qualifying products and more information about the scope of the ISBN here.
ISBNs, the law and copyright
The ISBN is an identifier and does not convey any form of legal or copyright protection. However, in some countries the use of ISBN to identify publications has been made into a legal requirement.
Who should apply for ISBN? 
It is always the publisher of the book who should apply for the ISBN. For the purposes of ISBN, the publisher is the group, organisation, company or individual who is responsible for initiating the production of a publication. Normally, it is also the person or body who bears the cost and financial risk in making a product available. It is not normally the printer, but it can be the author of the book if the author has chosen to publish their book themselves. 
In a number of countries there is detailed legislation regarding publishing so contact your national ISBN agency in good time for advice.


MySql

Install dmg

system preference -> mysql

sudo nano /etc/paths
/usr/local/mysql/bin 
/usr/local/mysql/support-files

env
echo 'export PATH=/usr/local/mysql/bin:$PATH' >> ~/.bash_profile
echo 'export PATH=/usr/local/mysql/support-files:$PATH' >> ~/.bash_profile

. ~/.bash_profile
mysql.server start

sudo chmod 7777 /usr/local/mysql/*

delete .pid and .err from /mysql/data/

sudo mysql.server start
mysql -u root -p




mysql> CREATE DATABASE mynewdb;
mysql> use mynewdb;
mysql> create table pet (name VARCHAR(20), owner VARCHAR(20), birth DATE, death DATE);
mysql> show tables;


